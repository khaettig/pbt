import unittest

from pbt.exploitation import Truncation


class TruncationTest(unittest.TestCase):
    def setUp(self):
        self.truncation = Truncation()

    def test_only_one_member(self):
        scores = {'member1': 1.0}

        result = self.truncation('member1', scores)

        self.assertEqual(result, 'member1')

    def test_two_members_with_same_name(self):
        scores = {'member1': 1.0, 'member2': 2.0}

        result = self.truncation('member2', scores)

        self.assertEqual(result, 'member2')

    def test_two_members_with_different_name(self):
        scores = {'member1': 1.0, 'member2': 2.0}

        result = self.truncation('member1', scores)

        self.assertEqual(result, 'member2')

    def test_three_members_with_same_name(self):
        scores = {'member1': 1.0, 'member2': 2.0, 'member3': 3.0}

        result = self.truncation('member3', scores)

        self.assertEqual(result, 'member3')

    def test_three_members_with_different_name(self):
        scores = {'member1': 1.0, 'member2': 2.0, 'member3': 3.0}

        result = self.truncation('member1', scores)

        self.assertEqual(result, 'member3')

    def test_many_members_with_same_name(self):
        scores = {
            'member1': 1.0, 'member2': 2.0, 'member3': 3.0, 'member4': 4.0,
            'member5': 5.0, 'member6': 6.0, 'member7': 7.0, 'member8': 8.0}

        result = self.truncation('member8', scores)

        self.assertEqual(result, 'member8')

    def test_many_members_with_different_name(self):
        scores = {
            'member1': 1.0, 'member2': 2.0, 'member3': 3.0, 'member4': 4.0,
            'member5': 5.0, 'member6': 6.0, 'member7': 7.0, 'member8': 8.0}

        result = self.truncation('member2', scores)

        self.assertIn(result, ['member7', 'member8'])

