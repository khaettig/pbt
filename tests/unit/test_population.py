from unittest.mock import MagicMock

from pyfakefs.fake_filesystem_unittest import TestCase

from pbt.population import Population


class SynchronousPopulationTest(TestCase):
    def setUp(self):
        self.population = Population(10, None, '/')
        self.population.members = [MagicMock() for i in range(10)]

    def test_get_next_member_time_step_zero(self):
        for member in self.population.members:
            member.is_free = True
            member.time_step = 0

        result = self.population.get_next_member()

        self.assertIsNotNone(result)

    def test_get_next_member_arbitrary_time_step_all_finished(self):
        for member in self.population.members:
            member.is_free = True
            member.time_step = 42

        result = self.population.get_next_member()

        self.assertIsNotNone(result)

    def test_get_next_member_not_all_finished(self):
        for member in self.population.members:
            member.is_free = False
            member.time_step = 0
        self.population.members[0].is_free = True
        self.population.members[0].time_step = 1

        result = self.population.get_next_member()

        self.assertIsNone(result)
