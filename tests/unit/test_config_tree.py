import unittest

from pbt.exploration.models.config_tree import ConfigTree
from pbt.exploration.models.config_tree.nodes import Categorical, Float


class ConfigTreeTest(unittest.TestCase):
    def test_returns_empty_dict_if_no_nodes_exist(self):
        tree = ConfigTree([])

        hyperparameters = tree.sample()

        self.assertEqual(0, len(hyperparameters))

    def test_returns_single_option_if_no_alternative_exists(self):
        tree = ConfigTree([Categorical('categorical', {'only_option': None})])

        hyperparameters = tree.sample()

        self.assertEqual(hyperparameters['categorical'], 'only_option')

    def _build_tree_with_categorical_and_children(self):
        return ConfigTree([
            Categorical('categorical', {
                'option 1': Float('float 1', low=0.0, high=1.0),
                'option 2': Float('float 2', low=1.0, high=2.0)}),
            Float('float 3', low=2.0, high=3.0)])

    def _assert_hyperparameters_have_valid_values(self, hyperparameters):
        if hyperparameters['categorical'] == 'option 1':
            self.assertIsNotNone(hyperparameters['float 1'])
            self.assertIsNone(hyperparameters['float 2'])
        elif hyperparameters['categorical'] == 'option 2':
            self.assertIsNone(hyperparameters['float 1'])
            self.assertIsNotNone(hyperparameters['float 2'])
        else:
            self.fail(f'Invalid value: "{hyperparameters["categorical"]}"')

    def test_can_handle_categorical_with_children(self):
        tree = self._build_tree_with_categorical_and_children()

        hyperparameters = tree.sample()

        self._assert_hyperparameters_have_valid_values(hyperparameters)

    def test_copy_copies_full_tree(self):
        tree1 = self._build_tree_with_categorical_and_children()

        tree2 = tree1.structural_copy()

        tree1.root = []
        hyperparameters = tree2.sample()
        self._assert_hyperparameters_have_valid_values(hyperparameters)
