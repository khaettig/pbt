import random
from pyfakefs.fake_filesystem_unittest import TestCase

from pbt.agents import ToyAgent
from pbt.exploitation import Truncation
from pbt.exploration import PerturbAndResample
from pbt.worker import Worker
from pbt.controller import Controller


class CorePBTTest(TestCase):
    def setUp(self):
        self.setUpPyfakefs()

    def test_synchronous_execution_with_one_worker(self):
        mutations = {
            'foo': lambda: 4.0 * random.random() - 2.0,
            'boost': lambda: random.choice(['a', 'b']),
            'bar': lambda: random.choice(range(101))}
        start_hyperparameters = {
            'foo': 0.01,
            'boost': 'a',
            'bar': 50}

        controller = Controller(
            pop_size=10,
            start_hyperparameters=start_hyperparameters,
            exploitation=Truncation(),
            exploration=PerturbAndResample(mutations),
            ready=lambda: True,
            stop=lambda iterations, _: iterations >= 100.0,
            data_path='/',
            results_path='/')

        worker = Worker(
            worker_id=0,
            agent=ToyAgent(
                decay=1.0,
                phase_len=100),
            data_path='/')

        worker.register(controller)
        worker.run()

        with open('/0/scores.txt', 'r') as f:
            scores = f.readlines()
        self.assertGreater(float(scores[-1]), 0.99)

