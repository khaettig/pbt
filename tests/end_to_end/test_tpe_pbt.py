from pbt.exploration.model_based import ModelBased
from pbt.exploration.models.config_tree import ConfigTree
from pbt.exploration.models.config_tree.nodes import Float, Integer, Categorical
from pyfakefs.fake_filesystem_unittest import TestCase

from pbt.agents import ToyAgent
from pbt.exploitation import Truncation
from pbt.exploration import TreeParzenEstimator
from pbt.worker import Worker
from pbt.controller import Controller


class TPEPBTTest(TestCase):
    def setUp(self):
        self.setUpPyfakefs()

    def test_synchronous_execution_with_one_worker_and_static_parameters(self):
        start_hyperparameters = {
            'foo': 0.01,
            'boost': 'a',
            'bar': 50}
        tree = ConfigTree([
            Float('foo', low=-2.0, high=2.0),
            Integer('bar', low=0, high=100),
            Categorical('boost', {'a': None, 'b': None})])

        controller = Controller(
            pop_size=10,
            start_hyperparameters=start_hyperparameters,
            exploitation=Truncation(),
            exploration=ModelBased(TreeParzenEstimator(tree)),
            ready=lambda: True,
            stop=lambda iterations, _: iterations >= 100.0,
            data_path='/',
            results_path='/')

        worker = Worker(
            worker_id=0,
            agent=ToyAgent(phase_len=100, foo_center=lambda time_step: 1.0),
            data_path='/')

        worker.register(controller)
        worker.run()

        with open('/0/scores.txt', 'r') as f:
            scores = f.readlines()
        self.assertGreater(float(scores[-1]), 0.98)

