import os
import shutil


class GarbageCollector:
    def __init__(self, data_path):
        self.data_path = data_path

    def collect(self, member_id):
        return # TODO: Reenable
        # Remove older models and keep the newest,
        # this assumes synchronous mode
        member_path = os.path.join(self.data_path, str(member_id))
        dirs = self._get_integer_dirs(member_path)
        dirs.remove(max(dirs))
        for old_dir in dirs:
            shutil.rmtree(str(old_dir))

    def _get_integer_dirs(self, path):
        return [int(i) for i in os.listdir(path) if i.isdigit()]
