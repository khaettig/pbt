import json
import os


class Member:
    def __init__(self, member_id, stop):
        self.member_id = member_id
        self.stop = stop

        self.trials = {}
        self.time_step = 0
        self.max_time_step = None
        self.last_score = 0

        self.is_free = True
        self.is_done = False

    def __repr__(self):
        return f'<Member id={self.member_id}, time_step={self.time_step}>'

    def assign_trial(self, trial, last_score):
        self.is_free = False
        self.trials[trial.time_step] = trial
        self.last_score = last_score

    def reached_time_step(self, time_step):
        if time_step not in self.trials:
            return False
        return self.trials[time_step].score is not None

    def get_last_score(self):
        if self.time_step == 0:
            return -float('inf')
        return self.trials[self.time_step - 1].score

    def get_hyperparameters(self):
        return self.trials[self.time_step - 1].hyperparameters

    def save_score(self, score):
        trial = self.trials[self.time_step]
        trial.score = score
        trial.improvement = score - self.last_score
        self.time_step += 1

        # TODO: Move this to own function
        if self.max_time_step:
            if self.time_step >= self.max_time_step:
                self.is_done = True
            else:
                self.is_free = True
        else:
            if self.stop(self.time_step, score):
                self.is_done = True
            else:
                self.is_free = True

        return trial

    def log_last_result(self, results_path):
        last_result = self.trials[self.time_step - 1]
        member_path = os.path.join(results_path, str(self.member_id))
        step_path = os.path.join(member_path, str(last_result.time_step))
        if not os.path.isdir(step_path):
            os.makedirs(step_path)
        with open(os.path.join(member_path, 'scores.txt'), 'a+') as f:
            f.write(f'{last_result.score}\n')
        with open(os.path.join(step_path, 'nodes.json'), 'w') as f:
            json.dump(last_result.hyperparameters, f)
        with open(os.path.join(step_path, 'add.json'), 'w') as f:
            json.dump(
                {'copied from': self.trials[self.time_step - 1].model_id}, f)

    def _create_hyperparameters(self):
        if self.time_step == 0:
            return self.exploration.get_start_hyperparameters()
        return self.exploration()
