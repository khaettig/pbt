import random


class Perturb:
    """
    A simple perturb mechanism as specified in (Jaderberg et al., 2017).
    """
    def __init__(self, boundaries={}):
        self.boundaries = boundaries

    def __call__(self, hyperparameters: dict) -> dict:
        """
        Perturb the nodes in the input.
        :param hyperparameters: A dict with nodes.
        :return: The perturbed nodes.
        """
        result = hyperparameters.copy()

        for key in hyperparameters:
            if isinstance(result[key], float):
                result[key] += random.choice([-1, 1]) * 0.2 * result[key]
            elif isinstance(result[key], int):
                result[key] += round(random.choice([-1, 1]) * 0.2 * result[key])
        self.ensure_boundaries(result)
        return result

    def ensure_boundaries(self, result):
        for key in result:
            if key not in self.boundaries:
                continue
            if result[key] < self.boundaries[key][0]:
                result[key] = self.boundaries[key][0]
            elif result[key] > self.boundaries[key][1]:
                result[key] = self.boundaries[key][1]
