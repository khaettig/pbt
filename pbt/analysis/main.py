import sys

from bokeh.io import curdoc
from bokeh.models.widgets import Tabs

from pbt.analysis.data import Data
from pbt.analysis.views import DataSetView, HyperparameterView, \
    ComparisonView, ScoresView


def load_specified_paths(load_function):
    if len(sys.argv) % 2 == 0:
        print('Error: Expected format:')
        print('<name 1> <path 1> <name 2> <path 2> ...')
        exit(1)
    names = [sys.argv[i] for i in range(1, len(sys.argv), 2)]
    paths = [sys.argv[i] for i in range(2, len(sys.argv), 2)]
    for name, path in zip(names, paths):
        load_function(name, path)


data = Data()

if len(sys.argv) > 1:
    load_specified_paths(data.load_data_set)


curdoc().add_root(Tabs(tabs=[
    DataSetView(data).widget, ComparisonView(data).widget,
    ScoresView(data).widget, HyperparameterView(data).widget]))

