from bokeh.models import CheckboxGroup
from pbt.analysis import event
from pbt.analysis.elements import Element


class ToggleLogAxis(Element):
    def __init__(self, data):
        super().__init__(data)

        self.checkbox = CheckboxGroup(
            labels=['Log X Axis', 'Log Y Axis'], active=[])
        self.checkbox.on_change('active', self.toggle)
        self.data.register_event(event.AXIS_CHANGED, self.update)

        self.widget = self.checkbox

    def update(self):
        active = []
        if self.data.log_x_axis:
            active.append(0)
        if self.data.log_y_axis:
            active.append(1)
        if active != self.checkbox.active:
            self.checkbox.active = active

    def toggle(self, attr, old, new):
        if 0 in new:
            self.data.log_x_axis = True
        else:
            self.data.log_x_axis = False
        if 1 in new:
            self.data.log_y_axis = True
        else:
            self.data.log_y_axis = False
