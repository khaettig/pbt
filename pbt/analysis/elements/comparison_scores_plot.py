import numpy as np
from collections import defaultdict

from bokeh.layouts import column
from bokeh.plotting import Figure
from bokeh.palettes import inferno
from pbt.analysis import event
from pbt.analysis.elements import Element


class ComparisonScoresPlot(Element):
    def __init__(self, data):
        super().__init__(data)

        self.data.register_event(event.COMPARISON_CHANGED, self.update)
        self.data.register_event(event.AXIS_CHANGED, self.update)

        self.widget = column(self._plot())

    def update(self):
        self.widget.children[0] = self._plot()

    def _plot(self):
        figure = Figure(
            title=self._get_figure_title(), plot_width=800,
            x_axis_label='Time Step', y_axis_label='Score',
            x_axis_type='log' if self.data.log_x_axis else 'linear',
            y_axis_type='log' if self.data.log_y_axis else 'linear')

        source_data = self._get_source_data()

        for name, curve in source_data.items():
            self._plot_single(figure, name, curve)

        if len(source_data) > 0:
            figure.legend.location = 'bottom_right'

        return figure

    def _plot_single(self, figure, name, curve):
        figure.line(
            curve['x'], curve['y_means'], color=curve['color'], legend=name,
            line_width=3)
        figure.patch(
            np.hstack((curve['x'], curve['x'][::-1])), curve['y_area'],
            color=curve['color'], fill_alpha=0.2)

    def _get_source_data(self):
        result = dict()
        data_sets = self.data.current_comparison_data_sets

        for data_set, color in zip(data_sets, inferno(len(data_sets) * 1.5)):
            combined = defaultdict(list)
            for values in data_set.scores.values():
                for pair in values:
                    combined[pair[0]].append(pair[1])
            x = [key for key in combined.keys()]
            y_means = [np.max(value) for value in combined.values()]
            stds = [np.std(value) for value in combined.values()]

            result[data_set.name] = {
                'x': x,
                'y_means': y_means,
                'y_area': np.hstack((
                    [mean + std for mean, std in zip(y_means, stds)],
                    [mean - std for mean, std in zip(y_means, stds)][::-1])),
                'color': color}
        return result

    def _get_figure_title(self):
        return f'Comparison'
