from bokeh.models import Select

from pbt.analysis import event
from pbt.analysis.elements import Element


class SelectDataSet(Element):
    def __init__(self, data):
        super().__init__(data)

        self.select = Select(
            title='Current Data Set:',
            value=self.data.current_data_set_name,
            options=self.data.get_data_set_names())
        self.select.on_change('value', self.select_data_set)
        self.data.register_event(event.DATA_SET_CHANGED, self.update)

        self.widget = self.select

    def select_data_set(self, attr, old, new):
        self.data.current_data_set_name = new

    def update(self):
        self.select.options = self.data.get_data_set_names()
        self.select.value = self.data.current_data_set_name
