from bokeh.models import Button

from pbt.analysis.elements import Element


class RemoveDataSet(Element):
    def __init__(self, data):
        super().__init__(data)

        self.button = Button(label='Remove Data Set', button_type='danger')
        self.button.on_click(self.data.remove_current_data_set)

        self.widget = self.button
