from bokeh.layouts import column, row
from bokeh.models import TextInput, Button

from pbt.analysis.elements import Element


class AddDataSet(Element):
    def __init__(self, data):
        super().__init__(data)

        self.text_name = TextInput(title='Name:')
        self.text_path = TextInput(title='Path:')
        button_submit = Button(label='Load Data Set', button_type='success')
        button_submit.on_click(self.load_data_set)

        self.widget = column(row(self.text_name, self.text_path), button_submit)

    def load_data_set(self):
        self.data.load_data_set(self.text_name.value, self.text_path.value)
