from bokeh.models import CheckboxGroup
from pbt.analysis.elements import Element


class ToggleHyperparameterJitter(Element):
    def __init__(self, data):
        super().__init__(data)

        self.checkbox = CheckboxGroup(labels=['Jitter'], active=[])
        self.checkbox.on_change('active', self.toggle_jitter)

        self.widget = self.checkbox

    def toggle_jitter(self, attr, old, new):
        if 0 in new:
            self.data.hyperparameter_jitter = True
        else:
            self.data.hyperparameter_jitter = False
