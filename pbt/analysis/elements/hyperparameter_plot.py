from bokeh.layouts import column
from bokeh.models import ColumnDataSource, Jitter
from bokeh.plotting import Figure
from bokeh.palettes import inferno
from pbt.analysis import event
from pbt.analysis.elements import Element


class HyperparameterPlot(Element):
    def __init__(self, data):
        super().__init__(data)

        self.plot_width = 800

        self.data.register_event(event.DATA_SET_CHANGED, self.update)
        self.data.register_event(event.HYPERPARAMETER_CHANGED, self.update)
        self.data.register_event(event.AXIS_CHANGED, self.update)

        self.source = None
        self.widget = column(self._plot())

    def update(self):
        self.widget.children[0] = self._plot()

    def _plot(self):
        self.source = ColumnDataSource(data=self._get_source_data())

        if self._is_categorical():
            return self._plot_categorical()
        return self._plot_continuous()

    def _is_categorical(self):
        if len(self.source.data['y']) == 0:
            return False

        sample = self.source.data['y'][0]
        return type(sample) is not int and type(sample) is not float

    def _plot_categorical(self):
        plot = Figure(
            title=self._get_figure_title(),
            plot_width=self.plot_width,
            x_axis_label='Time Step',
            y_axis_label='Value',
            y_range=list(set(self.source.data['y'])))

        plot.circle(x='x', y='y', color='color', source=self.source)

        return plot

    def _plot_continuous(self):
        plot = Figure(
            title=self._get_figure_title(),
            plot_width=self.plot_width,
            x_axis_label='Time Step',
            y_axis_label='Value',
            x_axis_type='log' if self.data.log_x_axis else 'linear',
            y_axis_type='log' if self.data.log_y_axis else 'linear')

        plot.circle(
            x='x',
            y={
                'field': 'y',
                'transform': Jitter(width=self._get_jitter_width())},
            color='color',
            source=self.source)

        return plot

    def _get_source_data(self):
        members = self.data.get_current_hyperparameter_values()

        if not members:
            return dict(x=[], y=[], color=[])

        palette = inferno(len(members))

        return dict(
            x=[pair[0] for values in members.values() for pair in values],
            y=[pair[1] for values in members.values() for pair in values],
            color=[
                palette[i]
                for i, values in enumerate(members.values())
                for _ in values])

    def _get_jitter_width(self):
        if self.data.hyperparameter_jitter:
            delta = max(self.source.data['y']) - min(self.source.data['y'])
            return delta / 50.0
        else:
            return 0.0

    def _get_figure_title(self):
        return f'Hyperparameter: {self.data.current_hyperparameter}'
