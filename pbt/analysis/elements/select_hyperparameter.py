from bokeh.models import Select

from pbt.analysis import event
from pbt.analysis.elements import Element


class SelectHyperparameter(Element):
    def __init__(self, data):
        super().__init__(data)

        self.select = Select(
            title='Hyperparameter:',
            options=self.data.get_hyperparameter_names())
        self.select.on_change('value', self.change_hyperparameter)
        self.data.register_event(event.DATA_SET_CHANGED, self.update)
        self.data.register_event(event.HYPERPARAMETER_CHANGED, self.update)

        self.widget = self.select

    def change_hyperparameter(self, attr, old, new):
        self.data.current_hyperparameter = new

    def update(self):
        self.select.options = self.data.get_hyperparameter_names()
