from bokeh.models import DataTable, TableColumn, ColumnDataSource

from pbt.analysis import event
from pbt.analysis.elements import Element


class DataSetInfo(Element):
    def __init__(self, data):
        super().__init__(data)

        self.table = DataTable(
            columns=[TableColumn(field='name', title='Name')],
            source=ColumnDataSource(self.data.get_current_data_set_info()))
        self.data.register_event(event.DATA_SET_CHANGED, self.update)

        self.widget = self.table

    def update(self):
        self.table.source.data.update(self.data.get_current_data_set_info())
