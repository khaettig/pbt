from bokeh.layouts import column
from bokeh.plotting import Figure
from bokeh.palettes import inferno
from pbt.analysis import event
from pbt.analysis.elements import Element


class ScoresPlot(Element):
    def __init__(self, data):
        super().__init__(data)

        self.data.register_event(event.DATA_SET_CHANGED, self.update)

        self.widget = column(self._plot())

    def update(self):
        self.widget.children[0] = self._plot()

    def _plot(self):
        figure = Figure(
            title=self._get_figure_title(), plot_width=800,
            x_axis_label='Time Step', y_axis_label='Score')

        members = self.data.get_current_scores()

        colors = inferno(int(len(members) * 1.0))

        names = sorted(members.keys())

        for name, color in zip(names, colors):
            figure.line(
                [pair[0] for pair in members[name]],
                [pair[1] for pair in members[name]],
                color=color, legend=str(name))

        if len(members) > 0:
            figure.legend.location = 'bottom_right'

        return figure

    def _get_figure_title(self):
        return f'Scores: {self.data.current_data_set_name}'
