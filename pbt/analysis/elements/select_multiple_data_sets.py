from bokeh.models import MultiSelect
from pbt.analysis import event

from pbt.analysis.elements import Element


class SelectMultipleDataSets(Element):
    def __init__(self, data):
        super().__init__(data)

        self.multi_select = MultiSelect(
            title='Datasets:',
            options=self.data.get_data_set_names(),
            value=self.data.current_comparison_names)
        self.multi_select.on_change('value', self.change_datasets)
        self.data.register_event(event.COMPARISON_CHANGED, self.update)

        self.widget = self.multi_select

    def change_datasets(self, attr, old, new):
        self.data.current_comparison_names = new

    def update(self):
        self.multi_select.value = self.data.current_comparison_names
