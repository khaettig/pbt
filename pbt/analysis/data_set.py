import json
import os
from collections import defaultdict


class DataSet:
    def __init__(self, path, name):
        self.path = path
        self.name = name
        self.scores = defaultdict(list)
        self.hyperparameters = defaultdict(lambda: defaultdict(list))

        for member_id in self._get_integer_dirs(path):
            self.load_member(member_id)

        self.current_hyperparameter = self.hyperparameter_names[0]

    @property
    def hyperparameter_names(self):
        return sorted(list(self.hyperparameters.keys()))

    def load_member(self, member_id):
        member_path = os.path.join(self.path, str(member_id))

        for time_step in self._get_integer_dirs(member_path):
            self.load_time_step(member_id, time_step)

        self.scores[member_id] = self.load_scores(member_path)

    def load_scores(self, member_path):
        with open(os.path.join(member_path, 'scores.txt')) as f:
            return [(i, float(line)) for i, line in enumerate(f.readlines())]

    def load_time_step(self, member_id, time_step):
        time_step_path = os.path.join(self.path, str(member_id), str(time_step))

        hyperparameters = json.load(
            open(os.path.join(time_step_path, 'nodes.json')))

        for name, value in hyperparameters.items():
            self.hyperparameters[name][member_id].append((time_step, value))

    def _get_integer_dirs(self, path):
        return [int(i) for i in os.listdir(path) if i.isdigit()]
