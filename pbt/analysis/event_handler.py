from collections import defaultdict


class EventHandler:
    def __init__(self):
        self.events = defaultdict(list)

    def register(self, event_name, function):
        self.events[event_name].append(function)

    def fire(self, event_name):
        for function in self.events[event_name]:
            function()
