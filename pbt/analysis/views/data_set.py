from bokeh.layouts import column
from bokeh.models import Panel

from pbt.analysis.elements import AddDataSet, SelectDataSet, DataSetInfo, \
    RemoveDataSet
from pbt.analysis.views import View


class DataSetView(View):
    def __init__(self, data):
        super().__init__(data)

        self.widget = Panel(title='Data Sets', child=column(
            AddDataSet(data).widget, SelectDataSet(data).widget,
            DataSetInfo(data).widget, RemoveDataSet(data).widget))
