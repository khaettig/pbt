from bokeh.layouts import column, row
from bokeh.models import Panel

from pbt.analysis.elements import SelectDataSet, ScoresPlot
from pbt.analysis.views import View


class ScoresView(View):
    def __init__(self, data):
        super().__init__(data)

        self.widget = Panel(title='Scores', child=row(
            column(SelectDataSet(data).widget),
            ScoresPlot(data).widget))
