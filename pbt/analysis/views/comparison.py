from bokeh.layouts import column, row
from bokeh.models import Panel
from pbt.analysis.elements import \
    ComparisonScoresPlot, SelectMultipleDataSets, ToggleLogAxis

from pbt.analysis.views import View


class ComparisonView(View):
    def __init__(self, data):
        super().__init__(data)

        self.widget = Panel(title='Comparison', child=row(
            column(
                SelectMultipleDataSets(data).widget,
                ToggleLogAxis(data).widget),
            ComparisonScoresPlot(data).widget))
