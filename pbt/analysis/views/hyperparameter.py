from bokeh.layouts import column, row
from bokeh.models import Panel

from pbt.analysis.elements import SelectDataSet, SelectHyperparameter, \
    HyperparameterPlot, ToggleHyperparameterJitter, ToggleLogAxis
from pbt.analysis.views import View


class HyperparameterView(View):
    def __init__(self, data):
        super().__init__(data)

        self.widget = Panel(title='Hyperparameters', child=row(
            column(
                SelectDataSet(data).widget,
                SelectHyperparameter(data).widget,
                ToggleHyperparameterJitter(data).widget,
                ToggleLogAxis(data).widget),
            HyperparameterPlot(data).widget))
