from pbt.analysis import event
from pbt.analysis.data_set import DataSet
from pbt.analysis.event_handler import EventHandler


class Data:
    def __init__(self):
        self._data_sets = {}
        self._event_handler = EventHandler()
        self._current_data_set_name = None
        self._current_comparison_names = []
        self._log_x_axis = False
        self._log_y_axis = False
        self._hyperparameter_jitter = False

    @property
    def current_data_set(self):
        return self._data_sets.get(self._current_data_set_name)

    @property
    def current_data_set_name(self):
        return self._current_data_set_name

    @current_data_set_name.setter
    def current_data_set_name(self, new):
        # Check if a update is really necessary (to prevent firing the same
        # event multiple times).
        if self._current_data_set_name == new:
            return

        current_hyperparameter = self.current_hyperparameter

        self._current_data_set_name = new

        if current_hyperparameter:
            self.current_data_set.current_hyperparameter = current_hyperparameter
        self._event_handler.fire(event.DATA_SET_CHANGED)
        self._event_handler.fire(event.HYPERPARAMETER_CHANGED)

    @property
    def current_comparison_names(self):
        return self._current_comparison_names

    @current_comparison_names.setter
    def current_comparison_names(self, new):
        # Check if a update is really necessary (to prevent firing the same
        # event multiple times).
        if self._current_comparison_names == new:
            return
        self._current_comparison_names = new
        self._event_handler.fire(event.COMPARISON_CHANGED)

    @property
    def current_comparison_data_sets(self):
        return [
            self._data_sets[name]
            for name in self.current_comparison_names]

    @property
    def current_hyperparameter(self):
        if self.current_data_set:
            return self.current_data_set.current_hyperparameter

    @current_hyperparameter.setter
    def current_hyperparameter(self, new):
        # Check if a update is really necessary (to prevent firing the same
        # event multiple times).
        if self.current_data_set.current_hyperparameter == new:
            return
        self.current_data_set.current_hyperparameter = new
        self._event_handler.fire(event.HYPERPARAMETER_CHANGED)

    @property
    def log_x_axis(self):
        return self._log_x_axis

    @log_x_axis.setter
    def log_x_axis(self, new):
        # Check if a update is really necessary (to prevent firing the same
        # event multiple times).
        if self._log_x_axis == new:
            return
        self._log_x_axis = new
        self._event_handler.fire(event.AXIS_CHANGED)

    @property
    def log_y_axis(self):
        return self._log_y_axis

    @log_y_axis.setter
    def log_y_axis(self, new):
        # Check if a update is really necessary (to prevent firing the same
        # event multiple times).
        if self._log_y_axis == new:
            return
        self._log_y_axis = new
        self._event_handler.fire(event.AXIS_CHANGED)

    @property
    def hyperparameter_jitter(self):
        return self._hyperparameter_jitter

    @hyperparameter_jitter.setter
    def hyperparameter_jitter(self, new):
        # Check if a update is really necessary (to prevent firing the same
        # event multiple times).
        if self.hyperparameter_jitter == new:
            return
        self._hyperparameter_jitter = new
        self._event_handler.fire(event.HYPERPARAMETER_CHANGED)

    def register_event(self, event_name, function):
        self._event_handler.register(event_name, function)

    def load_data_set(self, name, path):
        if name in self._data_sets:
            return

        self._data_sets[name] = DataSet(path, name)
        self.current_data_set_name = name
        self._event_handler.fire(event.COMPARISON_CHANGED)

    def remove_current_data_set(self):
        del self._data_sets[self.current_data_set_name]
        if len(self._data_sets) > 0:
            self.current_data_set_name = sorted(self._data_sets.keys())[0]
        else:
            self.current_data_set_name = None

    def get_data_set_names(self):
        return sorted(list(self._data_sets.keys()))

    def get_hyperparameter_names(self):
        if self.current_data_set:
            return sorted(list(self.current_data_set.hyperparameter_names))

    def get_current_hyperparameter_values(self):
        if self.current_data_set:
            return self.current_data_set.hyperparameters[
                self.current_hyperparameter]

    def get_current_scores(self):
        if self.current_data_set:
            return self.current_data_set.scores

    def get_current_data_set_info(self):
        # TODO: Move to data set
        if self.current_data_set_name:
            return {'name': [self.current_data_set_name]}
        else:
            return {}
