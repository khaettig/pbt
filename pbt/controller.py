import numpy as np

import random
import logging
import os

from pbt.exploitation import Truncation
from pbt.exploration import Perturb
from pbt.garbage_collector import GarbageCollector
from pbt.network import ControllerDaemon
from pbt.population import Population
from pbt.scheduler import Scheduler


class Controller:
    def __init__(
            self, pop_size, start_hyperparameters, exploitation=Truncation(),
            exploration=Perturb(), ready=lambda: True,
            stop=lambda iterations, _: iterations >= 100.0,
            data_path=os.getcwd(), results_path=os.getcwd()):
        self.logger = logging.getLogger('pbt')
        self.daemon = None
        self.population = Population(pop_size, stop, results_path)
        self.data_path = data_path
        self.scheduler = Scheduler(
            self.population, start_hyperparameters, exploitation, exploration)
        self.garbage_collector = GarbageCollector(data_path)
        self.ready = ready

        self.workers = {}
        self._done = False

        self.logger.info(
            f'Started controller with parameters: ' +
            f'population size: {pop_size}, '
            f'data_path: {data_path}, '
            f'results_path: {results_path}')

    def start_daemon(self):
        self.logger.info('Starting daemon.')
        self.daemon = ControllerDaemon(self)
        self.daemon.start()

    def register_worker(self, worker):
        self.logger.debug(f'Worker {worker.worker_id} registered.')
        self.workers[worker.worker_id] = worker

    def request_trial(self):
        return self.scheduler.get_trial().to_tuple()

    def send_evaluation(self, member_id, score):
        self.logger.debug(
            f'Receiving evaluation for member {member_id}: {score}')
        # TODO: Use ready function
        trial = self.population.update(member_id, score)
        self.scheduler.update_exploration(trial)
        self.garbage_collector.collect(member_id)
        if self.population.is_done():
            self.logger.info('Nothing more to do. Shutting down.')
            self._shut_down_workers()
            if self.daemon:
                self.daemon.shut_down()

    def _shut_down_workers(self):
        for worker in self.workers.values():
            worker.stop()
