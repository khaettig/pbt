import json
import math
import os

from pbt.agents import Agent


class ToyAgent(Agent):
    def __init__(
            self, velocity=0.5, decay=0.99, phase_len=34,
            foo_center=lambda time_step: math.sin(time_step * math.pi / 50.0),
            foo_weight=0.5, bar_weight=0.5):

        self.velocity = velocity
        self.decay = decay
        self.phase_len = phase_len
        self.foo_center = foo_center
        self.foo_weight = foo_weight
        self.bar_weight = bar_weight

        self.model = None
        self._hyperparameters = None

    @property
    def hyperparameters(self) -> dict:
        return self._hyperparameters

    @hyperparameters.setter
    def hyperparameters(self, value: dict):
        self._hyperparameters = value.copy()

    def step(self):
        self.model['progress'] *= self.decay
        modifier = \
            self.foo_weight * self._get_foo_value() + \
            self.bar_weight * self._get_bar_value()
        self.model['progress'] += \
            abs(self.velocity * (1.0 - self.model['progress']) * modifier)
        self.model['time_step'] += 1

    def evaluate(self) -> float:
        return self.model['progress']

    def initialize_model(self):
        self.model = {'progress': 0, 'time_step': 0}

    def save_model(self, path: str):
        with open(os.path.join(path, 'model.txt'), 'w') as f:
            json.dump(self.model, f)

    def load_model(self, path: str):
        with open(os.path.join(path, 'model.txt'), 'r') as f:
            self.model = json.load(f)

    def _get_foo_value(self):
        time_step = self.model['time_step']
        distance = abs(self.foo_center(time_step) - self.hyperparameters['foo'])
        return max(0.0, 1.0 - distance)

    def _phase(self):
        if self.model['time_step'] % (2 * self.phase_len) < self.phase_len:
            return 'a'
        else:
            return 'b'

    def _get_bar_value(self):
        bar = self.hyperparameters['bar']
        if self.hyperparameters['boost'] == self._phase() and bar <= 100:
            return self.hyperparameters['bar'] / 100.0
        return 0.0
