import abc


class Agent:
    """
    The agent to train by Population Based Training.
    """

    @property
    @abc.abstractmethod
    def hyperparameters(self) -> dict:
        """
        The current configuration of the rllib agent.
        :return: A dict with all nodes
        """
        raise NotImplementedError('This method has to be overwritten!')

    @hyperparameters.setter
    @abc.abstractmethod
    def hyperparameters(self, value: dict):
        """
        Set the nodes of the rllib agent.
        :param value: A dict with all nodes
        """
        raise NotImplementedError('This method has to be overwritten!')

    @abc.abstractmethod
    def step(self):
        """
        Perform one step of optimization.
        """
        raise NotImplementedError('This method has to be overwritten!')

    @abc.abstractmethod
    def evaluate(self) -> float:
        """
        Evaluate the current performance of the agent.
        :return: The current score
        """
        raise NotImplementedError('This method has to be overwritten!')

    def save_extra_data(self, path: str):
        """
        Overwrite this method if you want to save extra data during training.
        :param path: The path where to save the data.
        """
        pass

    @abc.abstractmethod
    def save_model(self, path: str):
        """
        Save the model of the agent on the specified path.
        :param path: The path to the checkpoint
        """
        raise NotImplementedError('This method has to be overwritten!')

    @abc.abstractmethod
    def load_model(self, path: str):
        """
        Load the model from a specified path.
        :param path: The path to the checkpoint
        """
        raise NotImplementedError('This method has to be overwritten!')

    def initialize_model(self):
        # TODO: Doc
        pass
