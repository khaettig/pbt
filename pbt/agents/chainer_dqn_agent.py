import gym
import chainer
import chainerrl

if __name__ == '__main__':
    env = gym.make('Pendulum-v0')
    env = chainerrl.wrappers.CastObservationToFloat32(env)

    time_step_limit = env.spec.tags.get(
        'wrapper_config.TimeLimit.max_episode_steps')

    observation_space = env.observation_space
    observation_size = observation_space.low.size
    action_space = env.action_space

    q_function = chainerrl.q_functions.FCQuadraticStateQFunction(
        observation_size, action_space.low.size,
        n_hidden_channels=2,
        n_hidden_layers=2,
        action_space=action_space)

    optimizer = chainer.optimizers.Adam()
    optimizer.setup(q_function)

    ou_sigma = (action_space.high - action_space.low) * 0.2
    explorer = chainerrl.explorers.AdditiveOU(sigma=ou_sigma)

    agent = chainerrl.agents.DQN(
        q_function, optimizer, chainerrl.replay_buffer.ReplayBuffer(10 ** 5),
        gamma=0.99, explorer=explorer, replay_start_size=1000,
        target_update_interval=10 ** 2, update_interval=1,
        minibatch_size=32, target_update_method='hard',
        soft_update_tau=1e-2)

    agent.load('.')

    chainerrl.experiments.train_agent(
        agent=agent, env=env, steps=10000, outdir='/tmp/crl')

    result = chainerrl.experiments.eval_performance(
        env=env, agent=agent, n_steps=None, n_episodes=10,
        max_episode_len=time_step_limit)

    agent.save('.')

    print(result)
