import numpy as np
from pbt.agents import Agent
from tensorforce.contrib.openai_gym import OpenAIGym
from tensorforce.agents import PPOAgent as TFPPOAgent
from tensorforce.execution import Runner


class PPOAgent(Agent):
    def __init__(self, env_name: str, hyperparameters: dict = {}, steps=100):
        """
        A PPO agent based on the tensorforce implementation.
        :param env_name: The name of the env to train on
        :param hyperparameters: The start nodes that differ from
        the standard configuration
        """
        self._env = OpenAIGym(env_name)
        self._hyperparameters = hyperparameters
        self._last_score = float('-inf')
        self._steps = steps

        self._model_path = None
        self._tensorforce_agent = None

    @property
    def hyperparameters(self) -> dict:
        return self._hyperparameters

    @hyperparameters.setter
    def hyperparameters(self, value: dict):
        self._hyperparameters = value
        if self._hyperparameters['lambda'] > 1.0:
            self._hyperparameters['lambda'] = 1.0

    def step(self):
        """
        Perform one step of optimization.
        """
        self._tensorforce_agent = self._setup_tf_agent()
        runner = Runner(agent=self._tensorforce_agent, environment=self._env)

        def save_score(r):
            self._last_score = np.mean(r.episode_rewards[-10:])
            return True

        runner.run(episodes=self._steps, max_episode_timesteps=200,
                   episode_finished=save_score)

    def evaluate(self):
        """
        Evaluate the current score of the agent.
        :return: The current score
        """
        return self._last_score

    def save_model(self, path: str):
        """
        Save the model of the agent on the specified path.
        :param path: The path to the checkpoint
        """
        self._tensorforce_agent.save_model(
            f'{path}/model', append_timestep=False)

    def load_model(self, path: str):
        """
        Load the model from a specified path.
        :param path: The path to the checkpoint
        """
        self._model_path = path

    def _setup_tf_agent(self):
        if self._tensorforce_agent:
            self._tensorforce_agent.close()

        network_spec = [
            # dict(type='embedding', indices=100, size=32),
            # dict(type'flatten'),
            dict(type='dense', size=256),
            dict(type='dense', size=128)
        ]

        agent = TFPPOAgent(
            states=self._env.states,
            actions=self._env.actions,
            network=network_spec,
            # Agent
            states_preprocessing=None,
            actions_exploration=None,
            reward_preprocessing=None,
            # MemoryModel
            update_mode=dict(
                unit='episodes',
                # 10 episodes per update
                batch_size=10,
                # Every 10 episodes
                frequency=10
            ),
            memory=dict(
                type='latest',
                include_next_states=False,
                capacity=5000
            ),
            # DistributionModel
            distributions=None,
            entropy_regularization=0.01,
            # PGModel
            baseline_mode='states',
            baseline=dict(
                type='mlp',
                sizes=[32, 32]
            ),
            baseline_optimizer=dict(
                type='multi_step',
                optimizer=dict(
                    type='adam',
                    learning_rate=self._hyperparameters['lr']
                ),
                num_steps=5
            ),
            gae_lambda=self._hyperparameters['lambda'],
            # PGLRModel
            likelihood_ratio_clipping=self._hyperparameters['clip_param'],
            # PPOAgent
            step_optimizer=dict(
                type='adam',
                learning_rate=self._hyperparameters['lr']
            ),
            subsampling_fraction=0.2,
            optimization_steps=25,
            execution=dict(
                type='single',
                session_config=None,
                distributed_spec=None
            )
        )

        if self._model_path:
            agent.restore_model(directory=self._model_path, file='model')

        return agent
