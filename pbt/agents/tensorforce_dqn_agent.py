from tensorforce.agents import PPOAgent
from tensorforce.execution import Runner
from tensorforce.contrib.openai_gym import OpenAIGym


# Callback function printing episode statistics
def episode_finished_p(r):
    print("Finished episode {ep} after {ts} timesteps (reward: {reward})".format(ep=r.episode, ts=r.episode_timestep,
                                                                                 reward=r.episode_rewards[-1]))
    return True


# Create an OpenAIgym environment.
environment = OpenAIGym('CartPole-v0', visualize=True)

# Network as list of layers
# - Embedding layer:
#   - For Gym environments utilizing a discrete observation space, an
#     "embedding" layer should be inserted at the head of the network spec.
#     Such environments are usually identified by either:
#     - class ...Env(discrete.DiscreteEnv):
#     - self.observation_space = spaces.Discrete(...)

# Note that depending on the following layers used, the embedding layer *may* need a
# flattening layer

network_spec = [
    # dict(type='embedding', indices=100, size=32),
    # dict(type'flatten'),
    dict(type='dense', size=32),
    dict(type='dense', size=32)
]


def get_agent():
    return PPOAgent(
        states=environment.states,
        actions=environment.actions,
        network=network_spec,
        # Agent
        states_preprocessing=None,
        actions_exploration=None,
        reward_preprocessing=None,
        # MemoryModel
        update_mode=dict(
            unit='episodes',
            # 10 episodes per update
            batch_size=10,
            # Every 10 episodes
            frequency=10
        ),
        memory=dict(
            type='latest',
            include_next_states=False,
            capacity=5000
        ),
        # DistributionModel
        distributions=None,
        entropy_regularization=0.01,
        # PGModel
        baseline_mode='states',
        baseline=dict(
            type='mlp',
            sizes=[32, 32]
        ),
        baseline_optimizer=dict(
            type='multi_step',
            optimizer=dict(
                type='adam',
                learning_rate=1e-3
            ),
            num_steps=5
        ),
        gae_lambda=0.97,
        # PGLRModel
        likelihood_ratio_clipping=0.2,
        # PPOAgent
        step_optimizer=dict(
            type='adam',
            learning_rate=1e-1
        ),
        subsampling_fraction=0.2,
        optimization_steps=25,
        execution=dict(
            type='single',
            session_config=None,
            distributed_spec=None
        )
    )

# agent.save_model('/tmp/tensorforce_test', append_timestep=False)

agent = get_agent()

runner = Runner(agent=agent, environment=environment)

result = []
agent.restore_model(directory='/tmp', file='tensorforce_test')


def episode_finished(r):
    print(f'{r.episode}')
    result.append(r.episode_rewards[-1])
    return True


runner.run(episodes=10, max_episode_timesteps=200,
           episode_finished=episode_finished)

print(result)

# runner.close()

# plot = figure()
# for result, color in zip(results, ['blue', 'red', 'green']):
#     plot.line(range(len(result)), result, color=color)
# show(plot)





# agent.save_model('/tmp/tensorforce_test', append_timestep=False)

# for i in range(5):
    # agent.restore_model(directory=directory, file=file)

    # Create the runner

# agent.restore_model(directory='/tmp', file='tensorforce_test')

# Start learning

#agent.optimizer['optimizer']['optimizer']['learning_rate'] = 0.1

# runner.run(episodes=10, max_episode_timesteps=200,
#            episode_finished=episode_finished)


# agent.save_model('/tmp/tensorforce_test', append_timestep=False)

runner.agent.close()

agent = get_agent()

runner = Runner(agent=agent, environment=environment)

result = []
agent.restore_model(directory='/tmp', file='tensorforce_test')


def episode_finished(r):
    print(f'{r.episode}')
    result.append(r.episode_rewards[-1])
    return True


runner.run(episodes=10, max_episode_timesteps=200,
           episode_finished=episode_finished)

print(result)

runner.close()
