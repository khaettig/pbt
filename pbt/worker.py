import numpy as np

import random
import logging
import os
from time import sleep

import Pyro4

from pbt.network import WorkerDaemon, ControllerAdapter, CONTROLLER_URI_FILENAME
from pbt.population import Trial


class Worker:
    # TODO: Doc, worker that sequentially calls step and evaluate
    def __init__(
            self, worker_id, agent, data_path=os.getcwd(), wait_time=5):
        self.logger = logging.getLogger('pbt')
        self.worker_id = worker_id
        self.agent = agent
        self.data_path = data_path
        self.wait_time = wait_time

        # TODO: _is_done
        self.is_done = False
        self._controller = None

    def register(self, controller=None):
        if controller:
            self.logger.info('Registered controller directly.')
            self._controller = controller
            self._controller.register_worker(self)
        else:
            self.logger.info('Registered controller over network.')
            self._controller = ControllerAdapter(self._discover_controller())
            self._run_daemon()

    def _run_daemon(self):
        self.logger.info('Starting worker daemon.')
        daemon = WorkerDaemon(self)
        uri = daemon.start()
        success = self._controller.register_worker_by_uri(uri)
        if not success:
            daemon.stop()
            raise Exception(f'The read controller URI "{uri}" is not valid!')

    def run(self):
        if not self._controller:
            self.register()

        while not self.is_done:
            self._run_iteration()

    def stop(self):
        self.logger.info('Shutting down worker.')
        self.is_done = True

    def _run_iteration(self):
        trial = self._load_trial()
        if not trial:
            return

        if trial.time_step == 0:
            self.agent.initialize_model()
        else:
            path = self._get_last_model_path(trial)
            self.agent.load_model(path)
        self.agent.hyperparameters = trial.hyperparameters
        self.agent.step()
        score = self.agent.evaluate()
        self.agent.save_model(self._create_model_path(trial))
        self.agent.save_extra_data(self._create_extra_data_path(trial))
        self._send_evaluation(trial.member_id, score)

    def _load_trial(self):
        trial = Trial.from_tuple(*self._get_trial())

        if not trial.is_valid():
            self.logger.info(
                f'No trial ready. Waiting for {self.wait_time} seconds.')
            sleep(self.wait_time)
        else:
            self.logger.info(f'Got valid trial {trial} from controller.')
            return trial

    def _discover_controller(self):
        self.logger.debug('Discovering controller.')
        file_path = os.path.join(self.data_path, CONTROLLER_URI_FILENAME)
        for number_of_try in range(5):
            try:
                with open(file_path, 'r') as f:
                    uri = f.readline().strip()
                break
            except FileNotFoundError:
                self.logger.info('Can\'t reach controller. Waiting ...')
                sleep(5)
                if number_of_try < 4:
                    continue
            raise Exception('Can\'t reach controller!')
        return Pyro4.Proxy(uri)

    def _get_trial(self):
        # TODO: Intercept connection issues
        return self._controller.request_trial()

    def _get_last_model_path(self, trial):
        # TODO: Remove ambiguity with model_id <-> member_id
        return os.path.join(
            self.data_path, str(trial.model_id), str(trial.model_time_step))

    def _create_model_path(self, trial):
        path = os.path.join(
            self.data_path, str(trial.member_id), str(trial.time_step))
        if not os.path.isdir(path):
            os.makedirs(path)
        return path

    def _create_extra_data_path(self, trial):
        path = os.path.join(
            self.data_path, 'extra', str(trial.member_id), str(trial.time_step))
        if not os.path.isdir(path):
            os.makedirs(path)
        return path

    def _send_evaluation(self, member_id, score):
        # TODO: Handle connection issues
        self.logger.info(f'Sending evaluation. Score: {score}')
        self._controller.send_evaluation(member_id, float(score))
