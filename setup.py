import subprocess
import sys

from setuptools import setup


def install_requirements():
    # Use pip for installing requirements, since it is less error-prone.
    subprocess.call(
        [sys.executable, '-m', 'pip', 'install', '-r', 'requirements.txt'])

install_requirements()

setup(
    name='pbt',
    version='1.0dev',
    author='Kevin Hättig',
    author_email='haettigk@cs.uni-freiburg.de',
    packages=['pbt'])
