# A dirty hack so that you don't have to install tensorforce

import sys, fakes

sys.modules['tensorforce.contrib.openai_gym'] = fakes
sys.modules['tensorforce.agents'] = fakes
sys.modules['tensorforce.execution'] = fakes

##############################################################################

import random

from pbt.agents import ToyAgent
from pbt.exploitation import Truncation
from pbt.exploration import PerturbAndResample
from pbt.worker import Worker
from pbt.controller import Controller


def main():
    print('Starting demo run ...')
    mutations = {
        'foo': lambda: 4.0 * random.random() - 2.0,
        'boost': lambda: random.choice(['a', 'b']),
        'bar': lambda: random.choice(range(101))}
    start_hyperparameters = {
        'foo': 0.01,
        'boost': 'a',
        'bar': 50}

    controller = Controller(
        pop_size=10,
        start_hyperparameters=start_hyperparameters,
        exploitation=Truncation(),
        exploration=PerturbAndResample(mutations),
        ready=lambda: True,
        stop=lambda iterations, _: iterations >= 300.0,
        data_path='./data',
        results_path='./results')

    worker = Worker(
        worker_id=0,
        agent=ToyAgent(phase_len=100),
        data_path='./data')

    worker.register(controller)
    worker.run()

    print(
        'Done! You can now see the results in the results folder.\n'
        'If you have bokeh installed you can use the analysis module to \n'
        'inspect the results by running the inspect_demo_data.sh script.')
    

if __name__ == '__main__':
    main()
