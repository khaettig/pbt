This is an implementation of the [Population Based Training](https://arxiv.org/abs/1711.09846) algorithm with some extensions that I implemented to base my Master's Thesis on. PBT is an algorithm to tune (potentionally non-stable) hyperparameters during a training run of a neural network.

# Usage
To see a quick run on a toy example that can be executed on a single machine, run the demo.sh script. (The used toy example simulates the training of a neural network but changes the ideal hyperparameters over time according to a sine function so that pbt has to adapt.)

# Structure
The software is split into two parts:

Controller:

* Keeps track of all workers and their status
* Searches for better hyperparameter settings
* Assigns trials to workers
  
Worker:

* Builds neural network and trains it
* Reports performance back to controller

For the communication I used [Pyro4](https://pythonhosted.org/Pyro4/) for transmitting hyperparameters and metadata as well as shared storage for the saved models of the neural networks.
