import numpy as np
import json
from bokeh.plotting import figure
from bokeh.io import show
from bokeh.models import Label


def main():
    with open('results.json', 'r') as f:
        results = json.load(f)

    plot = figure(
        title='TPE improvement over PBT (per step using explore)',
        x_axis_label='trial using explore (sorted)',
        y_axis_label='score improvement')
    plot.circle(range(len(results)), sorted([result[2] for result in results]))
    mean = np.mean([result[2] for result in results])
    plot.add_layout(Label(x=50, y=-0.05, text=f'Mean: {mean}'))
    show(plot)


if __name__ == '__main__':
    main()
