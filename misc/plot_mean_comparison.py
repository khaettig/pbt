import numpy as np
import os

import matplotlib.pyplot as plt

X = 1000
Y_LOW = -1200
Y_HIGH = 0


def load_score_file(path):
    with open(path, 'r') as f:
        return [float(x) for x in f.readlines()][:X]


def load_data(path, names, benchmarks):
    result = {}
    for name, benchmark in zip(names, benchmarks):
        name_results = []
        for i in range(1, 11):
            current_path = os.path.join(path, benchmark + '_' + str(i), 'results')
            scores = [
                load_score_file(
                    os.path.join(current_path, member, 'scores.txt'))
                for member in os.listdir(current_path)]
            run_scores = [
                np.max([score[l] for score in scores])
                for l in range(len(scores[0]))
            ]
            name_results.append(run_scores)
        result[name] = name_results
    return result


def main():
    data = load_data(
        '/data', ['Vanilla PBT', 'PBT + TPE'],
        ['final_pbt_walker', 'final_tpe_walker'])

    # plot = Figure(
    #     width=1600, height=600,
    #     x_range=(0, X), y_range=(Y_LOW, Y_HIGH),
    #     x_axis_label='time step', y_axis_label='episodic reward', tools='')
    # plot.toolbar.logo = None
    plt.figure(figsize=(9, 3), dpi=300)
    plt.xlabel('time step')
    plt.ylabel('episodic reward')
    plt.gca().set_xlim([0, X])

    for (name, scores), color in zip(data.items(), ['blue', 'red']):
        combined = [
            [score[l] for score in scores]
            for l in range(len(scores[0]))]
        x = range(len(combined))
        y_means = [np.mean(value) for value in combined]
        mins = [np.min(value) for value in combined]
        maxs = [np.max(value) for value in combined]
        y_area = np.hstack((
                [m for mean, m in zip(y_means, maxs)],
                [m for mean, m in zip(y_means, mins)][::-1]))
        plt.plot(x, y_means, color=color, label=name)
        plt.fill_between(x, maxs, y_means, color=color, alpha=0.3)
        plt.fill_between(x, y_means, mins, color=color, alpha=0.3)
        # plot.patch(
        #     np.hstack((x, x[::-1])), y_area, color=color, fill_alpha=0.2)
    plt.legend(loc='lower right')
    plt.gcf().subplots_adjust(bottom=0.15)
    plt.savefig('../../thesis/figures/experiments/walker_comp.png')


if __name__ == '__main__':
    main()
