from collections import defaultdict

import numpy as np
import os
import json

import matplotlib.pyplot as plt

HP_NAME = 'clip_param'
NAMES = ['Vanilla PBT', 'PBT + TPE']
BENCHMARKS = ['final_pbt_walker', 'final_tpe_walker']
X = 1000


def load_hyperparameters(path, result):
    for member in os.listdir(path):
        for time_step in os.listdir(os.path.join(path, member)):
            if not time_step.isnumeric():
                continue
            if int(time_step) >= X:
                continue
            hp_path = os.path.join(path, member, time_step, 'nodes.json')
            with open(hp_path, 'r') as f:
                hyperparameters = json.load(f)
            result[int(time_step)].append(hyperparameters[HP_NAME])


def load_data(path, names, benchmarks):
    result = {}
    for name, benchmark in zip(names, benchmarks):
        result[name] = defaultdict(list)
        for i in range(1, 11):
            current_path = os.path.join(path, benchmark + '_' + str(i), 'results')
            load_hyperparameters(current_path, result[name])
    return result


def main():
    data = load_data('/data', NAMES, BENCHMARKS)
    plt.figure(figsize=(3, 3), dpi=300)
    plt.xlabel('time step')
    plt.ylabel('clip')
    plt.gca().set_xlim([0, X])

    for benchmark, color in zip(NAMES, ['blue', 'red']):
        values = data[benchmark]
        combined = [
            values[time_step] for time_step in range(len(values))]
        x = range(len(combined))
        y_means = [np.mean(value) for value in combined]
        # y_means = savgol_filter(y_means, 51, 3)
        stds = [np.std(value) for value in combined]
        # stds = savgol_filter(stds, 51, 3)
        lower = [y - s for y, s in zip(y_means, stds)]
        print(lower)
        upper = [y + s for y, s in zip(y_means, stds)]
        print(upper)
        plt.plot(x, y_means, color=color, label=benchmark)
        plt.fill_between(x, upper, y_means, color=color, alpha=0.3)
        plt.fill_between(x, y_means, lower, color=color, alpha=0.3)
    # plt.legend()
    plt.gcf().subplots_adjust(bottom=0.15)
    plt.gcf().subplots_adjust(left=0.3)
    plt.savefig(
        f'../../thesis/figures/experiments/walker_{HP_NAME}.png')


if __name__ == '__main__':
    main()
