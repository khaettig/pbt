import os
import shutil


if __name__ == '__main__':
    dirs = os.listdir()
    if 'results' not in dirs or 'data' not in dirs or 'validation' in dirs:
        print('Now is not the time to use that!')
        exit(1)

    shutil.copytree('results', 'validation')

    for member_id in os.listdir('validation'):
        scores_file = os.path.join('validation', member_id, 'scores.txt')
        validation_file = os.path.join(
            'data/extra', member_id, 'validation_acc.txt')
        os.remove(scores_file)
        shutil.copyfile(validation_file, scores_file)
