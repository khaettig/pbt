import os
import json

from bokeh.plotting import figure
from bokeh.io import show
from bokeh.palettes import Category10


def load_scores_file(path):
    result = []
    with open(os.path.join(path, 'scores.txt'), 'r') as f:
        for line in f:
            result.append(float(line))
    # return result
    return [.0] + [result[i-1] - result[i] for i in range(1, len(result))]


def load_single_learning_rate(path):
    with open(os.path.join(path, 'nodes.json'), 'r') as f:
        hyperparameters = json.load(f)
    return hyperparameters['lr']


def load_learning_rates(path, time_steps=100):
    result = []
    for i in range(time_steps):
        result.append(load_single_learning_rate(os.path.join(path, str(i))))
    return result


def load_member(path):
    return load_learning_rates(path), load_scores_file(path)


if __name__ == '__main__':
    PATH = '/data/mnist_tpe_split_restricted'
    ALL_LRS = [[
        lr
        for lr in load_learning_rates(os.path.join(PATH, str(i)))]
        for i in range(10)]
    ALL_SCORES = [[
        score
        for score in load_scores_file(os.path.join(PATH, str(i)))]
        for i in range(10)]

    PLOT = figure(
        x_axis_label='learning rate', y_axis_label='improvement',
        x_axis_type='log')
    for i, color in zip(range(10), Category10[10]):
        print(i)
        print(color)
        PLOT.circle(ALL_LRS[i], ALL_SCORES[i], color=color)
    show(PLOT)
