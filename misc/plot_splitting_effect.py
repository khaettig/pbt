from bokeh.io import show, export_png
from bokeh.plotting import Figure
from bokeh.models import Span, Label, Arrow, NormalHead


DATA = [
    [1.0, 1.5, 1.7, 2.0, 3.0, 10.0, 25.0, 35.0, 42.0],
    [1.0, 1.3, 1.5, 1.8, 2.8, 8.5, 17.5, 27.0, 34.5],
    [1.0, 1.2, 1.3, 1.5, 2.6, 7.3, 15.0, 22.0, 27.0]
]

IMP_DATA = [
    [.5] + [curve[i+1] - curve[i] for i, x in enumerate(curve[:-1])]
    for curve in DATA]

print(IMP_DATA)


def split():
    l_tree_x = range(4, 9)
    l_tree_y = DATA[0][-5:]
    g_tree_x = list(range(len(DATA[1]))) + list(range(len(DATA[2])))
    g_tree_y = DATA[1] + DATA[2]

    plot = Figure(
        width=400, height=250,
        x_range=(0, 9), y_range=(0, 50),
        x_axis_label='Time Step', y_axis_label='Score')

    window_start = Span(
        location=0.8, dimension='height', line_dash='dashed', line_width=2)
    window_end = Span(
        location=8.2, dimension='height', line_dash='dashed', line_width=2)
    plot.add_layout(window_start)
    plot.add_layout(window_end)

    plot.add_layout(Arrow(
        start=NormalHead(size=10), end=NormalHead(size=10),
        x_start=0.8, y_start=47.0, x_end=8.2, y_end=47.0, line_width=2))
    plot.add_layout(Label(x=4.1, y=42, text='Sliding window'))

    for curve, color in zip(DATA, ['green', 'blue', 'red']):
        plot.line(range(len(curve)), curve, color=color, line_width=2)
    plot.circle(g_tree_x, g_tree_y, color='black', fill_color='white', size=8)
    plot.circle(l_tree_x, l_tree_y, color='black', fill_color='black', size=8)
    show(plot)


def no_split():
    l_tree_x = [
        x
        for curve in DATA for x, y in enumerate(curve)
        if x >= 1 and y >= 27.0]
    l_tree_y = [
        y for curve in DATA for x, y in enumerate(curve)
        if x >= 1 and y >= 27.0]
    g_tree_x = [
        x
        for curve in DATA for x, y in enumerate(curve)
        if x >= 1 and y < 27.0]
    g_tree_y = [
        y for curve in DATA for x, y in enumerate(curve)
        if x >= 1 and y < 27.0]

    plot = Figure(
        width=400, height=250,
        x_range=(0, 9), y_range=(0, 50),
        x_axis_label='Time Step', y_axis_label='Score')

    window_start = Span(
        location=0.8, dimension='height', line_dash='dashed', line_width=2)
    window_end = Span(
        location=8.2, dimension='height', line_dash='dashed', line_width=2)
    split_y = Span(
        location=26.0, dimension='width', line_width=2)
    plot.add_layout(window_start)
    plot.add_layout(window_end)
    plot.add_layout(split_y)
    plot.add_layout(Label(x=2.5, y=26.5, text='TPE split'))

    plot.add_layout(Arrow(
        start=NormalHead(size=10), end=NormalHead(size=10),
        x_start=0.8, y_start=47.0, x_end=8.2, y_end=47.0, line_width=2))
    plot.add_layout(Label(x=4.1, y=42, text='Sliding window'))

    for curve, color in zip(DATA, ['green', 'blue', 'red']):
        plot.line(range(len(curve)), curve, color=color, line_width=2)
    plot.circle(g_tree_x, g_tree_y, color='black', fill_color='white', size=8)
    plot.circle(l_tree_x, l_tree_y, color='black', fill_color='black', size=8)
    show(plot)


def improvement():
    l_tree_x = range(4, 9)
    l_tree_y = [1.0, 7.0, 15.0, 10.0, 7.5]
    g_tree_x = [
        x
        for curve in IMP_DATA for x, y in enumerate(curve)
        if x >= 1 and y not in l_tree_y] + [7, 8]
    g_tree_y = [
        y for curve in IMP_DATA for x, y in enumerate(curve)
        if x >= 1 and y not in l_tree_y] + [7.0, 7.0]

    plot = Figure(
        width=400, height=250,
        x_range=(0, 9), y_range=(0, 20),
        x_axis_label='Time Step', y_axis_label='Score Improvement')

    window_start = Span(
        location=0.8, dimension='height', line_dash='dashed', line_width=2)
    window_end = Span(
        location=8.2, dimension='height', line_dash='dashed', line_width=2)
    plot.add_layout(window_start)
    plot.add_layout(window_end)

    plot.add_layout(Arrow(
        start=NormalHead(size=10), end=NormalHead(size=10),
        x_start=0.8, y_start=19.0, x_end=8.2, y_end=19.0, line_width=2))
    plot.add_layout(Label(x=4.1, y=17, text='Sliding window'))

    for curve, color in zip(IMP_DATA, ['green', 'blue', 'red']):
        plot.line(range(len(curve)), curve, color=color, line_width=2)
    plot.circle(g_tree_x, g_tree_y, color='black', fill_color='white', size=8)
    plot.circle(l_tree_x, l_tree_y, color='black', fill_color='black', size=8)
    show(plot)


if __name__ == '__main__':
    # split()
    # no_split()
    improvement()
