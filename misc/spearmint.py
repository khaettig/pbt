import numpy as np
from scipy.stats import rankdata
from bokeh.plotting import figure
from bokeh.io import show

M5_TPE = [
    0.9654, 1.0033, 1.0229, 1.0383, 0.9603,
    0.9824, 1.0057, 0.9518, 0.9745, 1.0573]

M5_Real = [
    0.8996, 0.8983, 0.8976, 0.8982, 0.8977,
    0.8993, 0.8983, 0.8976, 0.8995, 0.8977]

M7_TPE = [
    0.9082, 0.9729, 0.8406, 0.9783, 0.8506,
    0.8188, 1.0988, 0.8737, 1.0371, 1.0522]
M7_Real = [
    0.8904, 0.8807, 0.8898, 0.8832, 0.8899,
    0.8900, 0.8906, 0.8904, 0.8891, 0.8902]


def plot_data(tpe, real):
    plot = figure(x_axis_label='TPE Score', y_axis_label='Real Score')

    plot.circle(tpe, real)

    show(plot)


def compute_spearmint(tpe, real):
    tpe_ranks = rankdata(tpe)
    real_ranks = len(real) - rankdata(real).astype(int) + 1
    print(f'Ranks: {tpe_ranks}, {real_ranks}')
    d_sum = np.sum([(x-y)**2 for x, y in zip(tpe_ranks, real_ranks)])
    print(1 - (d_sum / (len(tpe)**3 - len(tpe))))


if __name__ == '__main__':
    TPE = M7_TPE
    REAL = M7_Real

    plot_data(TPE, REAL)
    compute_spearmint(TPE, REAL)
