import random

from pbt.agents import PPOAgent
from pbt.controller import Controller
from pbt.worker import Worker

from pbt.exploitation import Truncation
from pbt.exploration import PerturbAndResample
from runs.run_base import get_parser, get_arguments


def main(args):
    mutations = {
        'lambda': lambda: random.uniform(0.9, 1.0),
        'clip_param': lambda: random.uniform(0.01, 0.5),
        'lr': [1e-3, 5e-4, 1e-4, 5e-5, 1e-5]}
    start_hyperparameters = {'lambda': 1.0, 'clip_param': 0.3, 'lr': 5e-5}

    controller = Controller(
        pop_size=args.pop_size,
        start_hyperparameters=start_hyperparameters,
        exploitation=Truncation(),
        exploration=PerturbAndResample(mutations),
        ready=lambda: True,
        stop=lambda iterations, _: iterations >= 10.0,
        data_path=args.data_path,
        results_path=args.results_path)

    worker = Worker(
        worker_id=args.job_id,
        agent=PPOAgent(
            env_name=args.env, hyperparameters=start_hyperparameters),
        data_path=args.data_path)

    worker.register(controller)
    worker.run()


if __name__ == '__main__':
    parser = get_parser()
    parser.add_argument('env', help='The env to run on.')
    arguments = get_arguments(parser)
    main(arguments)
