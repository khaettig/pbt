#!/bin/bash
#SBATCH -a 0-10
#SBATCH -p cpu_ivy
#SBATCH --mem 4000
#SBATCH -t 1-00:00 
#SBATCH -c 2
#SBATCH -o /home/haettigk/logs/%x.%N.%j.out
#SBATCH -e /home/haettigk/logs/%x.%N.%j.err
#SBATCH --mail-type=END,FAIL

/home/haettigk/anaconda3/envs/tensorflow1.13/bin/python3 run_pbt_ppo_env.py \
$SLURM_ARRAY_TASK_ID \
10 \
Taxi-v2 \
--data_path /home/haettigk/data/taxi/data \
--results_path /home/haettigk/data/taxi/results \
--temp_dir /tmp/haettigk/$SLURM_JOB_ID
--num_cpus 2
