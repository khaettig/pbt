import json
import os
import random
import tensorflow as tf
import numpy as np

from runs.misc.image_classifier_agent import ImageClassifierAgent
from pbt.exploration import PerturbAndResample


def main():
    data_path = '/home/haettigk/mnist_imp_3/data'
    results_path = '/home/haettigk/mnist_imp_3/results'
    differences = []

    mutations = {
        'lr': lambda: random.choice(
            [0.01, 0.005, 0.001, 0.0005, 0.0001, 0.00005, 0.00001])}

    load_function = tf.keras.datasets.fashion_mnist.load_data
    agent = ImageClassifierAgent(load_function)
    agent.initialize_model()
    strategy = PerturbAndResample(mutations)

    for member in os.listdir(results_path):
        with open(os.path.join(results_path, member, 'scores.txt')) as f:
            tpe_scores = [float(score) for score in f.readlines()]
        for time_step in os.listdir(os.path.join(results_path, member)):
            if not time_step.isnumeric() or int(time_step) == 0:
                continue
            with open(os.path.join(
                    results_path, member, time_step, 'add.json'), 'r') as f:
                additional = json.load(f)
            if additional['copied from'] != int(member):
                before = str(int(time_step) - 1)
                model_path = os.path.join(
                    data_path, str(additional['copied from']), before)
                param_path = os.path.join(
                    results_path, str(additional['copied from']), before)
                pbt_score = run_pbt_evaluation(
                    agent, strategy, model_path, param_path)
                differences.append(
                    (member, time_step, tpe_scores[int(time_step)] - pbt_score))
    with open('/home/haettigk/final_results.json', 'w') as f:
        json.dump(differences, f)


def run_pbt_evaluation(agent, strategy, model_path, param_path):
    agent.load_model(model_path)
    with open(os.path.join(param_path, 'nodes.json'), 'r') as f:
        hyperparameters = json.load(f)
    agent.hyperparameters = strategy(hyperparameters)
    agent.step()
    return agent.evaluate()

def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    tf.set_random_seed(seed)


if __name__ == '__main__':
    main()
