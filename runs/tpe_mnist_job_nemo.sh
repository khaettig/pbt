#!/bin/bash
#MSUB -l nodes=1:ppn=1
#MSUB -l walltime=00:5:00:00
#MSUB -l pmem=4000mb
#MSUB -t [0-10]
#MSUB -m bea
#MSUB -M haettigk@cs.uni-freiburg.de

/home/fr/fr_fr/fr_kh143/anaconda3/envs/tensorflow1.13/bin/python3 /home/fr/fr_fr/fr_kh143/masters_thesis/src/runs/run_tpe_image_classification.py \
$MOAB_JOBARRAYINDEX \
10 \
--data_path /work/ws/nemo/fr_kh143-pbt-0/data/mnist_tpe_1/data \
--results_path /work/ws/nemo/fr_kh143-pbt-0/data/mnist_tpe_1/results \
--logging_level DEBUG \
--logging_file /work/ws/nemo/fr_kh143-pbt-0/logs/tpe_mnist_1.log
