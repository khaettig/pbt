import numpy as np
import tensorflow as tf
from runs.misc.image_classifier_agent import ImageClassifierAgent

if __name__ == '__main__':
    np.random.seed(1)

    load_function = tf.keras.datasets.fashion_mnist.load_data
    agent = ImageClassifierAgent(load_function)

    print(agent.train_labels)
    print(agent.test_labels)
    print(agent.validation_labels)
