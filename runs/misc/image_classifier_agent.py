import os
import random
import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split

from pbt.agents import Agent


class ImageClassifierAgent(Agent):
    def __init__(self, data_set_load_function, seed=None):
        self.model = None
        self.seed = seed
        self._load_data(data_set_load_function)

    def initialize_model(self):
        self.model = tf.keras.Sequential([
            tf.keras.layers.Conv2D(
                filters=32, kernel_size=3, padding='valid', activation='relu',
                input_shape=(28, 28, 1)),
            tf.keras.layers.Conv2D(
                filters=32, kernel_size=3, padding='valid', activation='relu'),
            tf.keras.layers.MaxPool2D(pool_size=2),
            tf.keras.layers.Dropout(0.2),

            tf.keras.layers.Conv2D(
                filters=64, kernel_size=3, padding='same', activation='relu'),
            tf.keras.layers.Conv2D(
                filters=64, kernel_size=3, padding='same', activation='relu'),
            tf.keras.layers.MaxPool2D(pool_size=2),
            tf.keras.layers.Dropout(0.25),

            tf.keras.layers.Conv2D(
                filters=128, kernel_size=2, padding='same', activation='relu'),
            tf.keras.layers.MaxPool2D(pool_size=2),
            tf.keras.layers.Dropout(0.25),

            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(128, activation='relu'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(0.25),
            tf.keras.layers.Dense(10, activation='softmax')])

        self.model.compile(
            optimizer='adam',
            loss='sparse_categorical_crossentropy',
            metrics=['accuracy'])

    def set_seed(self, seed):
        tf.random.set_random_seed(seed)

    @property
    def hyperparameters(self) -> dict:
        learning_rate = tf.keras.backend.get_value(self.model.optimizer.lr)
        return {'lr': float(learning_rate)}

    @hyperparameters.setter
    def hyperparameters(self, value: dict):
        tf.keras.backend.set_value(self.model.optimizer.lr, value['lr'])

    def step(self):
        self.model.fit(
            self.train_images,
            self.train_labels,
            batch_size=64,
            epochs=1,
            verbose=False)

    def evaluate(self) -> float:
        test_loss, test_accuracy = self.model.evaluate(
            self.test_images, self.test_labels, verbose=False)
        return test_accuracy

    def save_extra_data(self, path: str):
        validation_loss, validation_accuracy = self.model.evaluate(
            self.validation_images, self.validation_labels, verbose=False)
        with open(os.path.join(path, '..', 'validation_acc.txt'), 'a+') as f:
            f.write(f'{validation_accuracy}\n')

    def save_model(self, path: str):
        tf.keras.models.save_model(
            model=self.model,
            filepath=self._get_model_path(path))

    def load_model(self, path: str):
        tf.keras.backend.clear_session()
        self.model = tf.keras.models.load_model(
            filepath=self._get_model_path(path))

    def merge_hyperparameters(self, hyperparameters):
        self.hyperparameters = hyperparameters

    def _get_model_path(self, dir_path):
        return os.path.join(dir_path, 'model.hdf5')

    def _load_data(self, data_set_load_function):
        (to_split_images, to_split_labels), (test_images, test_labels) = \
            data_set_load_function()

        train_images, validation_images, train_labels, validation_labels = \
            train_test_split(to_split_images, to_split_labels, test_size=1/6)

        self.train_images = train_images.reshape(
            train_images.shape[0], 28, 28, 1) / 255.0
        self.test_images = test_images.reshape(
            test_images.shape[0], 28, 28, 1) / 255.0
        self.validation_images = validation_images.reshape(
            validation_images.shape[0], 28, 28, 1) / 255.0
        self.train_labels = train_labels
        self.test_labels = test_labels
        self.validation_labels = validation_labels

        if self.seed:
            random.seed(self.seed)
            np.random.seed(self.seed)
            tf.set_random_seed(self.seed)
