#!/bin/bash
#MSUB -l nodes=1:ppn=1
#MSUB -l walltime=00:24:00:00
#MSUB -l pmem=4000mb
#MSUB -m bea
#MSUB -M haettigk@cs.uni-freiburg.de

/home/fr/fr_fr/fr_kh143/anaconda3/envs/tensorflow1.13/bin/python3 /home/fr/fr_fr/fr_kh143/masters_thesis/src/runs/run_pbt_comparison.py
