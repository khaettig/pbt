import argparse
import logging
import os
import shutil


def get_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument('job_id', help='The cluster job id.', type=int)
    parser.add_argument('pop_size', help='The population size.', type=int)
    parser.add_argument(
        '--data_path', help='The path to the folder used for communication.',
        default=os.getcwd())
    parser.add_argument(
        '--results_path', help='The path to the folder for the results.',
        default=os.getcwd())
    parser.add_argument(
        '--delete_previous', action='store_true',
        help='Delete previous contents of data and results folder.')
    parser.add_argument(
        '--logging_level', help='The level of verbosity of the log.',
        choices=['ERROR', 'WARNING', 'INFO', 'DEBUG'], default='WARNING')
    parser.add_argument(
        '--logging_file', help='Where to save the log file.')
    parser.add_argument(
        '--seed', type=int, default=1, help='The used seed for the whole run.')
    parser.add_argument(
        '--iterations', type=int, default=100)

    return parser


def get_arguments(parser):
    logger = logging.getLogger('pbt')

    logging.basicConfig()

    args, unknowns = parser.parse_known_args()

    if unknowns:
        raise ValueError(f'Unknown arguments: {unknowns}')

    if args.job_id == 0:
        formatter = logging.Formatter(
            '%(asctime)s Controller %(levelname)s: %(message)s')
    else:
        formatter = logging.Formatter(
            f'%(asctime)s Worker {args.job_id} %(levelname)s: %(message)s')

    if args.logging_file:
        handler = logging.FileHandler(args.logging_file)
        handler.setFormatter(formatter)
        for h in logger.handlers[:]:
            logger.removeHandler(h)
        logger.addHandler(handler)
    else:
        raise Exception('Logging_file is needed at the moment.')

    logger.setLevel(args.logging_level)

    if args.job_id == 0 and args.delete_previous:
        delete_if_exists(args.data_path)
        delete_if_exists(args.results_path)

    logger.debug('Starting run.')

    return args


def delete_if_exists(path):
    if os.path.isdir(path):
        shutil.rmtree(path)
