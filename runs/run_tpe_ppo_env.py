import numpy as np
import tensorflow as tf
import random
from pbt import Controller, Worker

from pbt.exploitation import Truncation
from pbt.agents import PPOAgent
from pbt.exploration import TreeParzenEstimator
from pbt.exploration.model_based import ModelBased
from pbt.exploration.models.config_tree import ConfigTree
from pbt.exploration.models.config_tree.nodes import Float, Integer
from runs.run_base import get_parser, get_arguments


def run_controller(args):
    set_seed(args.seed)

    tree = ConfigTree([
        Float('lambda', low=0.9, high=1.0),
        Float('clip_param', low=0.01, high=0.5),
        Float('lr', low=1e-5, high=1e-3)])
    start_hyperparameters = {
        'lambda': 1.0,
        'clip_param': 0.3,
        'lr': 5e-5}

    controller = Controller(
        pop_size=args.pop_size,
        start_hyperparameters=start_hyperparameters,
        exploitation=Truncation(),
        exploration=ModelBased(TreeParzenEstimator(
            tree, mode=args.mode, split=args.split)),
        ready=lambda: True,
        stop=lambda iterations, _: iterations >= args.iterations,
        data_path=args.data_path,
        results_path=args.results_path)
    controller.start_daemon()


def run_worker(args):
    set_seed(args.seed * args.job_id)

    worker = Worker(
        worker_id=args.job_id,
        agent=PPOAgent(env_name=args.env),
        data_path=args.data_path)
    worker.run()


def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    tf.set_random_seed(seed)


def main(args):
    if args.job_id == 0:
        run_controller(args)
    else:
        run_worker(args)


if __name__ == '__main__':
    parser = get_parser()
    parser.add_argument('env', help='The env to run on.')
    parser.add_argument('--mode', default='improvement')
    parser.add_argument('--split', default='time_step')

    arguments = get_arguments(parser)
    main(arguments)
