#!/bin/bash
#MSUB -l nodes=1:ppn=1
#MSUB -l walltime=00:4:00:00
#MSUB -l pmem=4000mb
#MSUB -t [55-109]
#MSUB -m bea
#MSUB -M haettigk@cs.uni-freiburg.de

NPOP=10
JOBID=$((MOAB_JOBARRAYINDEX % (NPOP + 1)))
RUNID=$((MOAB_JOBARRAYINDEX / (NPOP + 1) + 1))

/home/fr/fr_fr/fr_kh143/anaconda3/envs/tensorflow1.13/bin/python3 /home/fr/fr_fr/fr_kh143/masters_thesis/src/runs/run_pbt_ppo_env.py \
$JOBID \
$NPOP \
Acrobot-v1 \
--data_path /work/ws/nemo/fr_kh143-pbt-0/data/pbt_acrobot_$RUNID/data \
--results_path /work/ws/nemo/fr_kh143-pbt-0/results/pbt_acrobot_$RUNID/results \
--logging_level DEBUG \
--logging_file /work/ws/nemo/fr_kh143-pbt-0/logs/pbt_acrobot_$RUNID.log \
--seed $RUNID \
--iterations 500
