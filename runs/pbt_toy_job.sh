#!/bin/bash
#SBATCH -a 0-20
#SBATCH -p cpu_ivy
#SBATCH --mem 4000
#SBATCH -t 1-00:00 
#SBATCH -c 1
#SBATCH -o /home/haettigk/logs/%x.%N.%j.out
#SBATCH -e /home/haettigk/logs/%x.%N.%j.err
#SBATCH --mail-type=END,FAIL

/home/haettigk/anaconda3/envs/tensorflow1.13/bin/python3 run_tpe_toy_example.py \
$SLURM_ARRAY_TASK_ID \
20 \
--data_path /home/haettigk/data/toy/data \
--results_path /home/haettigk/data/toy/results
