import random
from pbt.controller import Controller
from pbt.worker import Worker

from pbt.exploitation import Truncation
from pbt.exploration import PerturbAndResample
from pbt.agents.toy_agent import ToyAgent
from runs.run_base import get_parser, get_arguments


def main(args):
    mutations = {
        'foo': lambda: 4.0 * random.random() - 2.0,
        'boost': lambda: random.choice(['a', 'b']),  # , 'null']),
        'bar': lambda: random.choice(range(101))}
    start_hyperparameters = {
        'foo': 0.01,
        'boost': 'a',
        'bar': 50}
    boundaries = {
        'foo': (-2.0, 2.0),
        'bar': (0, 100)
    }

    controller = Controller(
        pop_size=args.pop_size,
        start_hyperparameters=start_hyperparameters,
        exploitation=Truncation(),
        exploration=PerturbAndResample(mutations, boundaries=boundaries),
        ready=lambda: True,
        stop=lambda iterations, _: iterations >= 200.0,
        data_path=args.data_path,
        results_path=args.results_path)

    worker = Worker(
        worker_id=args.job_id,
        agent=ToyAgent(),
        data_path=args.data_path)

    worker.register(controller)
    worker.run()


if __name__ == '__main__':
    parser = get_parser()
    arguments = get_arguments(parser)
    main(arguments)
