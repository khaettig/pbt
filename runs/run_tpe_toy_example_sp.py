import numpy as np

from pbt.exploration.model_based import ModelBased
from pbt.exploration.models.config_tree import ConfigTree
from pbt.exploration.models.config_tree.nodes import Float, Integer, Categorical
from pbt.controller import Controller
from pbt.worker import Worker

from pbt.exploitation import Truncation
from pbt.exploration import TreeParzenEstimator
from pbt.agents.toy_agent import ToyAgent
from runs.run_base import get_parser, get_arguments


def main(args):
    start_hyperparameters = {
        'foo': lambda: np.random.uniform(low=-2.0, high=2.0),
        'boost': lambda: np.random.choice(['a', 'b']),
        'bar': lambda: np.round(np.random.uniform(low=0, high=100))}
    tree = ConfigTree([
        Float('foo', low=-2.0, high=2.0),
        Integer('bar', low=0, high=100),
        Categorical('boost', {'a': None, 'b': None})])

    controller = Controller(
        pop_size=args.pop_size,
        start_hyperparameters=start_hyperparameters,
        exploitation=Truncation(),
        exploration=ModelBased(
            TreeParzenEstimator(tree, mode=args.mode, split=args.split)),
        ready=lambda: True,
        stop=lambda iterations, _: iterations >= 200.0,
        data_path=args.data_path,
        results_path=args.results_path)

    worker = Worker(
        worker_id=args.job_id,
        agent=ToyAgent(),
        data_path=args.data_path)

    worker.register(controller)
    worker.run()


if __name__ == '__main__':
    parser = get_parser()
    parser.add_argument('--mode', default='improvement')
    parser.add_argument('--split', default='time_step')

    arguments = get_arguments(parser)
    main(arguments)
