#!/bin/bash
#SBATCH -a 0-10
#SBATCH -p cpu_ivy
#SBATCH --mem 4000
#SBATCH -t 1-00:00 
#SBATCH -c 2
#SBATCH -o /home/haettigk/logs/%x.%N.%j.out
#SBATCH -e /home/haettigk/logs/%x.%N.%j.err
#SBATCH --mail-type=END,FAIL

/home/haettigk/anaconda3/envs/tensorflow1.13/bin/python3 run_pbt_taxi_test.py \
$SLURM_ARRAY_TASK_ID \
10 \
--data_path /home/haettigk/data/atari_test/data \
--results_path /home/haettigk/data/atari_test/results \
--temp_dir /tmp/haettigk/$SLURM_JOB_ID \
--num_cpus 2
