import numpy as np
import tensorflow as tf
import random

from pbt import Controller, Worker
from pbt.exploitation import Truncation
from pbt.exploration import TreeParzenEstimator
from pbt.exploration.model_based import ModelBased
from pbt.exploration.models.config_tree import ConfigTree
from pbt.exploration.models.config_tree.nodes import LogFloat
from runs.misc.image_classifier_agent import ImageClassifierAgent
from runs.run_base import get_parser, get_arguments


def run_controller(args):
    tree = ConfigTree([LogFloat('lr', low=1e-5, high=1e-2)])
    start_hyperparameters = {'lr': 1e-2}

    controller = Controller(
        pop_size=args.pop_size,
        start_hyperparameters=start_hyperparameters,
        exploitation=Truncation(),
        exploration=ModelBased(TreeParzenEstimator(tree, mode=args.tpe_mode)),
        ready=lambda: True,
        stop=lambda iterations, _: iterations >= args.iterations,
        data_path=args.data_path,
        results_path=args.results_path)
    controller.start_daemon()


def run_worker(args):
    load_function = tf.keras.datasets.fashion_mnist.load_data

    worker = Worker(
        worker_id=args.job_id,
        agent=ImageClassifierAgent(load_function, args.seed * args.job_id),
        data_path=args.data_path)
    worker.run()


def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    tf.set_random_seed(seed)


def main(args):
    set_seed(args.seed)

    if args.job_id == 0:
        run_controller(args)
    else:
        run_worker(args)


if __name__ == '__main__':
    parser = get_parser()

    parser.add_argument(
        '--tpe_mode', help='The mode to run TPE in.',
        choices=['total', 'improvement'], default='improvement')

    arguments = get_arguments(parser)
    main(arguments)
