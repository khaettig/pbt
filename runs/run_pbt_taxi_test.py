import os
import logging
import random
import ray

from pbt.agents import PPOAgent
from pbt.controller import Controller
from pbt.worker import Worker

from pbt.exploitation import Truncation
from pbt.exploration import PerturbAndResample
from runs.run_base import get_parser, get_arguments

from gym.envs.atari.atari_env import AtariEnv
from ray.tune.registry import register_env


def env_creator(env_config):
    return CustomAtariEnv()


def register_custom_taxi():
    register_env('CustomAtari-v0', env_creator)


class CustomAtariEnv(AtariEnv):
    def step(self, a):
        logging.warning('STEP')
        return super().step(a)


def main(args):
    os.environ['TUNE_RESULT_DIR'] = f'{args.temp_dir}/ray_results'

    ray.init(
        num_cpus=args.num_cpus,
        object_store_memory=int(4e+9),
        temp_dir=args.temp_dir)

    mutations = {
        'lambda': lambda: random.uniform(0.9, 1.0),
        'clip_param': lambda: random.uniform(0.01, 0.5),
        'lr': [1e-3, 5e-4, 1e-4, 5e-5, 1e-5],
        'num_sgd_iter': lambda: random.randint(1, 30),
        'sgd_minibatch_size': lambda: random.randint(128, 16384),
        'train_batch_size': lambda: random.randint(2000, 160000)}
    start_hyperparameters = {
        'lambda': 1.0, 'clip_param': 0.3, 'lr': 5e-5, 'num_sgd_iter': 30,
        'sgd_minibatch_size': 128, 'train_batch_size': 4000}

    controller = Controller(
        pop_size=args.pop_size,
        start_hyperparameters=start_hyperparameters,
        exploitation=Truncation(),
        exploration=PerturbAndResample(mutations),
        ready=lambda: True,
        stop=lambda iterations, _: iterations >= 100.0,
        data_path=args.data_path,
        results_path=args.results_path)

    register_custom_taxi()

    worker = Worker(
        worker_id=args.job_id,
        agent=PPOAgent(env_name='CustomAtari-v0'),
        data_path=args.data_path)

    worker.register(controller)
    worker.run()


if __name__ == '__main__':
    logging.basicConfig(level='INFO')
    parser = get_parser()
    parser.add_argument(
        '--num_cpus', help='Number of cpus.', default=4, type=int)
    arguments = get_arguments(parser)
    main(arguments)
