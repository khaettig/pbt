import numpy as np
import tensorflow as tf

from runs.misc.image_classifier_agent import ImageClassifierAgent


def main():
    results = []
    model_path = '/data/mnist_tpe_debug/data/4/15'
    samples = [
        {'lr': 5.184656768923198e-06}, {'lr': 0.0032905721427994245},
        {'lr': 1.2563801920595378e-06}, {'lr': 0.0028086322280447807},
        {'lr': 0.00011187434852120286}, {'lr': 0.00030658002242035933},
        {'lr': 1.1508664132854843e-05}, {'lr': 0.00025041060852327144},
        {'lr': 0.00044945502819338667}, {'lr': 4.869115620221733e-06}]

    load_function = tf.keras.datasets.fashion_mnist.load_data
    agent = ImageClassifierAgent(load_function)
    agent.initialize_model()

    for hyperparameter in samples:
        print(f'Evaluating: {hyperparameter}')
        s = []
        agent.hyperparameters = hyperparameter

        agent.step()
        s.append(agent.evaluate())
        print(s)
        results.append(np.mean(s))
    print(results)


if __name__ == '__main__':
    main()
