import numpy as np
import tensorflow as tf
import random
from pbt import Controller, Worker

from pbt.exploitation import Truncation
from pbt.exploration import PerturbAndResample
from pbt.agents import PPOAgent
from runs.run_base import get_parser, get_arguments


def run_controller(args):
    set_seed(args.seed)

    mutations = {
        'lambda': lambda: random.uniform(0.9, 1.0),
        'clip_param': lambda: random.uniform(0.01, 0.5),
        'lr': [1e-3, 5e-4, 1e-4, 5e-5, 1e-5]}
    start_hyperparameters = {
        'lambda': 1.0,
        'clip_param': 0.3,
        'lr': 5e-5}
    boundaries = {
        'lambda': (0.9, 1.0),
        'clip_param': (0.01, 0.5),
        'lr': (1e-5, 1e-3)}

    controller = Controller(
        pop_size=args.pop_size,
        start_hyperparameters=start_hyperparameters,
        exploitation=Truncation(),
        exploration=PerturbAndResample(mutations, boundaries=boundaries),
        ready=lambda: True,
        stop=lambda iterations, _: iterations >= args.iterations,
        data_path=args.data_path,
        results_path=args.results_path)
    controller.start_daemon()


def run_worker(args):
    set_seed(args.seed * args.job_id)

    worker = Worker(
        worker_id=args.job_id,
        agent=PPOAgent(env_name=args.env),
        data_path=args.data_path)
    worker.run()


def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    tf.set_random_seed(seed)


def main(args):
    if args.job_id == 0:
        run_controller(args)
    else:
        run_worker(args)


if __name__ == '__main__':
    parser = get_parser()
    parser.add_argument('env', help='The env to run on.')
    arguments = get_arguments(parser)
    main(arguments)
