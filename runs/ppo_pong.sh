#!/bin/bash
#SBATCH -p cpu_ivy
#SBATCH --mem 8000
#SBATCH -t 1-00:00 
#SBATCH -c 2
#SBATCH -o /home/haettigk/logs/%x.%N.%j.out
#SBATCH -e /home/haettigk/logs/%x.%N.%j.err
#SBATCH --mail-type=END,FAIL

/home/haettigk/anaconda3/envs/tensorflow1.13/bin/python3 run_ppo_pong.py
